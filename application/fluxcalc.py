import datetime

from scipy.stats import linregress
from statsmodels.tsa.stattools import acf
from collections import namedtuple

from application.models import Datum

Fluxvalues = namedtuple("Fluxvalues",
                        ["lin_flux","intercept","slope","residual_sum","residual_ar"])

coefs = {
    'vm': 0.0224,
    'zt': 273.15,
    'convf': 3600.0,
    'molmass_co2': 44.0095,
    'molmass_ch4': 16.0425,
    'molmass_n2o': 44.0130
}

def ar_coefs(s):
    acfs = acf(s,nlags=4)
    return sum(acfs[1:5])

def co2_lin_flux(data,temp_b,temp_e,cvol,area,trim_b,trim_e,date):
    size = len(data)
    init_time = data[0].time
    init = datetime.datetime.combine(date,init_time)
    data_trimmed = data[trim_b:(size - trim_e)]
    trimsize = len(data_trimmed)
    secs = [(datetime.datetime.combine(date,data_trimmed[i].time) - init).seconds
            for i in range(trimsize)]
    co2 = [data_trimmed[i].co2_ppm for i in range(trimsize)]
    fit = linregress(secs,co2)
    residuals = [co2[i] - (fit.intercept + fit.slope * secs[i])
                 for i in range(len(secs))]
    residual_sum = sum(map(abs,residuals))
    residual_ar = ar_coefs(residuals)
    try:
        temp = (temp_b + temp_e)/2.0
    except:
        temp = temp_b
    volume = cvol/1000.0 # to m3
    lflux = fit.slope * (((coefs['molmass_co2']/coefs['vm']) *
                          (coefs['zt']/(coefs['zt'] + temp)) *
                          (volume/area) * coefs['convf']) /
                         1000.0)
    fv = Fluxvalues(lflux,fit.intercept,fit.slope,residual_sum,residual_ar)
    return fv

def ch4_lin_flux(data,temp_b,temp_e,cvol,area,trim_b,trim_e,date):
    size = len(data)
    init_time = data[0].time
    init = datetime.datetime.combine(date,init_time)
    data_trimmed = data[trim_b:(size - trim_e)]
    trimsize = len(data_trimmed)
    secs = [(datetime.datetime.combine(date,data_trimmed[i].time) - init).seconds
            for i in range(trimsize)]
    ch4 = [data_trimmed[i].ch4_ppb for i in range(trimsize)]
    fit = linregress(secs,ch4)
    residuals = [ch4[i] - (fit.intercept + fit.slope * secs[i])
                 for i in range(len(secs))]
    residual_sum = sum(map(abs,residuals))
    residual_ar = ar_coefs(residuals)
    try:
        temp = (temp_b + temp_e)/2.0
    except:
        temp = temp_b
    volume = cvol/1000.0 # to m3
    lflux = fit.slope * (((coefs['molmass_ch4']/coefs['vm']) *
                          (coefs['zt']/(coefs['zt'] + temp)) *
                          (volume/area) * coefs['convf']))
    fv = Fluxvalues(lflux,fit.intercept,fit.slope,residual_sum,residual_ar)
    return fv

def n2o_lin_flux(data,temp_b,temp_e,cvol,area,trim_b,trim_e,date):
    size = len(data)
    init_time = data[0].time
    init = datetime.datetime.combine(date,init_time)
    data_trimmed = data[trim_b:(size - trim_e)]
    trimsize = len(data_trimmed)
    secs = [(datetime.datetime.combine(date,data_trimmed[i].time) - init).seconds
            for i in range(trimsize)]
    n2o = [data_trimmed[i].n2o_ppb for i in range(trimsize)]
    fit = linregress(secs,n2o)
    residuals = [n2o[i] - (fit.intercept + fit.slope * secs[i])
                 for i in range(len(secs))]
    residual_sum = sum(map(abs,residuals))
    residual_ar = ar_coefs(residuals)
    try:
        temp = (temp_b + temp_e)/2.0
    except:
        temp = temp_b
    volume = cvol/1000.0 # to m3
    lflux = fit.slope * (((coefs['molmass_n2o']/coefs['vm']) *
                          (coefs['zt']/(coefs['zt'] + temp)) *
                          (volume/area) * coefs['convf']))
    fv = Fluxvalues(lflux,fit.intercept,fit.slope,residual_sum,residual_ar)
    return fv
