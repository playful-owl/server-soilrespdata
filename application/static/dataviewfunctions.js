'use strict';

$('document').ready( () => {

  $(".switch_button").click(function() {
    let id_string = $(this).attr('id');
    let id_num = parseInt(id_string.substring(10));
    let target = "/ajax/switch_bad/";
    target += id_num;
    $.ajax({
      type: "GET",
      url: target,
      success: function(msg){
	let badtext = "#badtext";
	badtext += id_num;
	$(badtext).text(msg.new_value);
	let badbuttontext = "#badbuttontext";
	badbuttontext += id_num;
	if(msg.new_value){
	  $(badbuttontext).text("good");
	}else{
	  $(badbuttontext).text("bad");
	}
      },
      error: function(msg){
	console.log(msg);
      }
    });
  });

  $(".trim_s_button").click(function() {
    let id_string = this.id;
    let id_num = parseInt(id_string.substring(13));
    let input_id = "#trim_s_entry";
    input_id += id_num;
    let value = parseInt($(input_id).val());
    //console.log("called trim_s_button id ",id_num," value ",value);
    set_trim_start(id_num,value);
  });

  $(".trim_e_button").click(function() {
    let id_string = this.id;
    let id_num = parseInt(id_string.substring(13));
    let input_id = "#trim_e_entry";
    input_id += id_num;
    let value = parseInt($(input_id).val());
    //console.log("called trim_e_button id ",id_num," value ",value);
    set_trim_end(id_num,value);
  });

  $(".recalculate_flux_button").click(function() {
     let id_string = this.id;
     let id_num = parseInt(id_string.substring(14));
     let input_start_id = "#trim_s_entry";
     input_start_id += id_num;
     let start_value = parseInt($(input_start_id).val());
     let input_end_id = "#trim_e_entry";
     input_end_id += id_num;
     let end_value = parseInt($(input_end_id).val());
     //console.log("recalculating ",id_num," with start ",start_value," end ",end_value);
     $.ajax({
       type: "POST",
       url: "/ajax/recalculate",
       contentType: 'application/json;charset=UTF-8',
       data: JSON.stringify({'series_id': id_num,
			     'trim_start_value': start_value,
			     'trim_end_value': end_value}),
       success: function(msg){
	 console.log(msg);
	 let headtext = "#trim_head_value";
	 headtext += id_num;
	 $(headtext).text(start_value);
	 let tailtext = "#trim_tail_value";
	 tailtext += id_num;
	 $(tailtext).text(end_value);
	 let fluxtextco2 = "#fluxtextco2";
	 fluxtextco2 += id_num;
	 $(fluxtextco2).text(msg.new_lin_flux_co2);
	 let fluxtextch4 = "#fluxtextch4";
	 fluxtextch4 += id_num;
	 $(fluxtextch4).text(msg.new_lin_flux_ch4);
	 let fluxtextn2o = "#fluxtextn2o";
	 fluxtextn2o += id_num;
	 $(fluxtextn2o).text(msg.new_lin_flux_n2o);
	 let rsumtextco2 = "#rsumtextco2";
	 rsumtextco2 += id_num;
	 $(rsumtextco2).text(msg.new_rsum_co2);
	 let rsumtextch4 = "#rsumtextch4";
	 rsumtextch4 += id_num;
	 $(rsumtextch4).text(msg.new_rsum_ch4);
	 let rsumtextn2o = "#rsumtextn2o";
	 rsumtextn2o += id_num;
	 $(rsumtextn2o).text(msg.new_rsum_n2o);
	 let rartextco2 = "#rartextco2";
	 rartextco2 += id_num;
	 $(rartextco2).text(msg.new_rar_co2);
	 let rartextch4 = "#rartextch4";
	 rartextch4 += id_num;
	 $(rartextch4).text(msg.new_rar_ch4);
	 let rartextn2o = "#rartextn2o";
	 rartextn2o += id_num;
	 $(rartextn2o).text(msg.new_rar_n2o);
	 set_slope(id_num,
		   msg.new_intercept_co2,
		   msg.new_intercept_ch4,
		   msg.new_intercept_n2o,
		   msg.new_slope_co2,
		   msg.new_slope_ch4,
		   msg.new_slope_n2o);
       },
       error: function(msg){
	 console.log(msg);
       }
     });
  });

  function set_trim_start(id,v){
    let index = -1;
    for(let i = 0; i < mat.length; i++){
      if(mat[i].id == id){
	index = i;
      }
    }
    if(index < 0){
      console.log("error in set_trim_start");
      return;
    }
    if(mat[index]['co2']){
      let target_line_co2 = "#trim_h_co2_";
      target_line_co2 += id;
      let tline = d3.select(target_line_co2);
      tline.attr("x1", x_axes[index](0 + v))
	.attr("x2", x_axes[index](0 + v));
    }
    if(mat[index]['ch4']){
      let target_line_ch4 = "#trim_h_ch4_";
      target_line_ch4 += id;
      let tline = d3.select(target_line_ch4);
      tline.attr("x1", x_axes[index](0 + v))
	.attr("x2", x_axes[index](0 + v));
    }
    if(mat[index]['n2o']){
      let target_line_n2o = "#trim_h_n2o_";
      target_line_n2o += id;
      let tline = d3.select(target_line_n2o);
      tline.attr("x1", x_axes[index](0 + v))
	.attr("x2", x_axes[index](0 + v));
    }
  }

  function set_trim_end(id,v){
    let index = -1;
    for(let i = 0; i < mat.length; i++){
      if(mat[i].id == id){
	index = i;
      }
    }
    if(index < 0){
      console.log("error in set_trim_end");
      return;
    }
    if(mat[index]['co2']){
      let target_line_co2 = "#trim_t_co2_";
      target_line_co2 += id;
      let tline = d3.select(target_line_co2);
      tline.attr("x1", x_axes[index](x_maxs[index] - v))
	.attr("x2", x_axes[index](x_maxs[index] - v));
    }
    if(mat[index]['ch4']){
      let target_line_ch4 = "#trim_t_ch4_";
      target_line_ch4 += id;
      let tline = d3.select(target_line_ch4);
      tline.attr("x1", x_axes[index](x_maxs[index] - v))
	.attr("x2", x_axes[index](x_maxs[index] - v));
    }
    if(mat[index]['n2o']){
      let target_line_n2o = "#trim_t_n2o_";
      target_line_n2o += id;
      let tline = d3.select(target_line_n2o);
      tline.attr("x1", x_axes[index](x_maxs[index] - v))
	.attr("x2", x_axes[index](x_maxs[index] - v));
    }
  }

  function set_slope(id,ic_co2,ic_ch4,ic_n2o,sl_co2,sl_ch4,sl_n2o){
    let index = -1;
    for(let i = 0; i < mat.length; i++){
      if(mat[i].id == id){
	index = i;
      }
    }
    if(index < 0){
      console.log("error in set_slope");
      return;
    }
    if(mat[index]['co2']){
      let target_line_co2 = "#slope_co2_";
      target_line_co2 += id;
      let tline = d3.select(target_line_co2);
      let new_y1 = y_axes_co2[index](ic_co2);
      let new_y2 = y_axes_co2[index](ic_co2 + x_maxs[index] * sl_co2);
      tline.attr("y1", y_axes_co2[index](ic_co2))
	.attr("y2", y_axes_co2[index](ic_co2 + x_maxs[index] * sl_co2));
    }
    if(mat[index]['ch4']){
      let target_line_ch4 = "#slope_ch4_";
      target_line_ch4 += id;
      let tline = d3.select(target_line_ch4);
      let new_y1 = y_axes_ch4[index](ic_ch4);
      let new_y2 = y_axes_ch4[index](ic_ch4 + x_maxs[index] * sl_ch4);
      tline.attr("y1", y_axes_ch4[index](ic_ch4))
	.attr("y2", y_axes_ch4[index](ic_ch4 + x_maxs[index] * sl_ch4));
    }
    if(mat[index]['n2o']){
      let target_line_n2o = "#slope_n2o_";
      target_line_n2o += id;
      let tline = d3.select(target_line_n2o);
      let new_y1 = y_axes_n2o[index](ic_n2o);
      let new_y2 = y_axes_n2o[index](ic_n2o + x_maxs[index] * sl_n2o);
      tline.attr("y1", y_axes_n2o[index](ic_n2o))
	.attr("y2", y_axes_n2o[index](ic_n2o + x_maxs[index] * sl_n2o));
    }
  }
  
});

