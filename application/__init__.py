from flask import Flask
from config import configuration
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager

server = Flask(__name__)
server.config.from_object(configuration)
db = SQLAlchemy(server)
migrate = Migrate(server,db)
login = LoginManager(server)
login.login_view = 'login'

from application import routes, models
