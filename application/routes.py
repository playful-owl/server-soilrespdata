import os
import pandas
import datetime
import requests

##from scipy.stats import linregress

from flask import (
    flash,
    jsonify,
    make_response,
    render_template,
    redirect,
    request,
    Response,
    send_from_directory,
    url_for,
)

from config import fileserver_url

from application import server, db
from application.models import User, Available, Measurements, Series, Datum
from application.forms import Loginform

from application.with_authenticate import fetch_url
from application.dbhandler import *
from application.fluxcalc import *

from flask_login import login_required, current_user, login_user, logout_user

@server.route('/js/<path:path>')
def js(path):
    return send_from_directory('js',path)

@server.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = Loginform()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        return redirect(url_for('index'))
    return render_template('login.html', title='Sign In', form=form)

@server.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))

@server.route('/')
@server.route('/index',methods=['GET'])
@login_required
def index():
    avail = Available.query.order_by(Available.measure_date).all()
    meas = Measurements.query.order_by(Measurements.siteids,Measurements.measure_date).all()
    seri = Series.query.filter_by(empty=False).order_by(Series.siteid,Series.date,Series.start_time).all()
    return render_template('index.html', sub_data=avail, meas=meas, seri=seri)

@server.route('/maintenance',methods=['GET'])
@login_required
def maintenance():
    avail = Available.query.order_by(Available.measure_date).all()
    meas = Measurements.query.order_by(Measurements.siteids,Measurements.measure_date).all()
    seri = Series.query.filter_by(empty=False).order_by(Series.siteid,Series.date,Series.start_time).all()
    return render_template('maintenance.html', sub_data=avail, meas=meas, seri=seri)

@server.route('/downloads',methods=['GET'])
@login_required
def downloads():
    seri = Series.query.filter_by(empty=False).order_by(Series.siteid,Series.date,Series.start_time).all()
    siteids_all = [s.siteid for s in seri]
    siteids = list(set(siteids_all))
    data = [{'siteid': s,
             'count': siteids_all.count(s)}
            for s in siteids]
    return render_template('downloads.html', data=data)

## route for fetching a dataframe from the database
@server.route('/api/dataframe/<siteid>')
def dataframe(siteid):
    q0 = Series.query.filter_by(siteid=siteid,empty=False).statement
    df = pandas.read_sql_query(q0,db.session.bind)
    cdisp = "attachment; filename=export_" + siteid + ".csv"
    return Response(df.to_csv(), mimetype="text/csv", headers={"Content-disposition": cdisp})

## api routes for fetching data from the file server

@server.route('/api/update')
def update():
    try:
        num_rows_deleted = db.session.query(Available).delete()
        db.session.commit()
    except:
        db.session.rollback()
    target = fileserver_url + 'submissions/'
    rdata = fetch_url(target)
    if rdata is not None:
        for i in range(len(rdata)):
            new_available(rdata[i])
    return redirect("/maintenance")

@server.route('/api/fetch/<id>')
def fetch(id):
    meas = Measurements.query.filter_by(fileserver_id=id).all()
    if len(meas) > 1:
        flash("error: multiple Measurements with this id " + id)
        return redirect("/index")
    elif len(meas) == 1:
        flash("Mesurement set " + id + " already present")
        return redirect("/index")
    target = fileserver_url + 'submission/' + id + '/'
    meas_data = fetch_url(target)
    if meas_data is None:
        flash("Error: received no measurements data")
        return redirect("/index")
    new_meas = new_measurements(meas_data) ## adds to db session
    if not new_meas.get('ok'):
        flash("Error: could not insert Measurements to database")
        return redirect("/index")
    new_meas_id = new_meas.get('local_id')
    assoc_series = Series.query.filter_by(measurements=new_meas_id).all()
    if len(assoc_series) > 0:
        flash("Error: fetched measurement " + new_meas_id +
              " already associated with " + str(len(assoc_series)) + " series")
    else:
        ## got Meas, now fetch series
        flash("fetched Measurement set")
        target = fileserver_url + 'field/' + id + '/'
        series_data = fetch_url(target)
        if not series_data:
            flash("Error: received no series data")
            return redirect("/index")
        new_s = new_series(series_data,new_meas_id) ## adds to db session
        ## set siteids for the Measurements set
        new_meas['meas'].siteids = " ".join(new_s['siteids'])
        ## got series, now fetch points
        for i in range(len(new_s['ids'])):
            target = fileserver_url + 'dataseries/' + id + '/' + str(i) + '/'
            point_data = fetch_url(target)
            if point_data is None:
                flash("Error: received no point data")
                return redirect("/index")
            new_d = new_data(point_data,new_s['ids'][i]) ## adds to db session
            if new_d['empty']:
                new_s['objs'][i].empty = True
            else:
                new_s['objs'][i].empty = False
                if 'co2' in new_d['gases']:
                    new_s['objs'][i].co2 = True
                if 'ch4' in new_d['gases']:
                    new_s['objs'][i].ch4 = True
                if 'n2o' in new_d['gases']:
                    new_s['objs'][i].n2o = True
    avail = Available.query.filter_by(fileserver_id=id).first()
    avail.fetched = True
    new_meas['meas'].successful = True
    db.session.commit() ## this should be the only commit
    return redirect("/maintenance")

## fetching fileserver data from shell
@server.route('/api/fetch2/<id>')
def fetch2(id):
    meas = Measurements.query.filter_by(fileserver_id=id).all()
    if len(meas) > 1:
        print("error: multiple Measurements with this id " + id)
        return False
    elif len(meas) == 1:
        print("Mesurement set " + id + " already present")
        return False
    target = fileserver_url + 'submission/' + id + '/'
    meas_data = fetch_url(target)
    if meas_data is None:
        print("Error: received no measurements data")
        return False
    new_meas = new_measurements(meas_data) ## adds to db session
    if not new_meas.get('ok'):
        print("Error: could not insert Measurements to database")
        return False
    new_meas_id = new_meas.get('local_id')
    assoc_series = Series.query.filter_by(measurements=new_meas_id).all()
    if len(assoc_series) > 0:
        print("Error: fetched measurement " + new_meas_id +
              " already associated with " + str(len(assoc_series)) + " series")
    else:
        ## got Meas, now fetch series
        print("fetched Measurement set")
        target = fileserver_url + 'field/' + id + '/'
        series_data = fetch_url(target)
        if series_data is None:
            print("Error: received no series data")
            return False
        new_s = new_series(series_data,new_meas_id) ## adds to db session
        ## set siteids for the Measurements set
        new_meas['meas'].siteids = " ".join(new_s['siteids'])
        ## got series, now fetch points
        for i in range(len(new_s['ids'])):
            target = fileserver_url + 'dataseries/' + id + '/' + str(i) + '/'
            point_data = fetch_url(target)
            if point_data is None:
                print("Error: received no point data")
                return False
            new_d = new_data(point_data,new_s['ids'][i]) ## adds to db session
            if new_d['empty']:
                new_s['objs'][i].empty = True
            else:
                new_s['objs'][i].empty = False
                if 'co2' in new_d['gases']:
                    new_s['objs'][i].co2 = True
                if 'ch4' in new_d['gases']:
                    new_s['objs'][i].ch4 = True
                if 'n2o' in new_d['gases']:
                    new_s['objs'][i].n2o = True
    avail = Available.query.filter_by(fileserver_id=id).first()
    avail.fetched = True
    new_meas['meas'].successful = True
    db.session.commit()
    return True

@server.route('/api/forget/<local_id>')
def forget(local_id):
    print('forget')
    meas = Measurements.query.filter_by(id=local_id).all()
    if(len(meas) == 1):
        flash("forgetting Measurement set")
        fsid = meas[0].fileserver_id
        assoc_series = Series.query.filter_by(measurements=local_id).all()
        for i in range(len(assoc_series)):
            series = assoc_series[i]
            assoc_data = Datum.query.filter_by(series=series.id).all()
            for j in range(len(assoc_data)):
                datum = assoc_data[j]
                db.session.delete(datum)
            db.session.delete(series)
        db.session.delete(meas[0])
        avail = Available.query.filter_by(fileserver_id=fsid).first()
        avail.fetched = False
        db.session.commit()
    elif(len(meas) == 0):
        flash("Mesurement set " + local_id + " not present")
    else:
        flash("error: multiple Measurements with this local id " + local_id)
    return redirect("/maintenance")

@server.route('/dataview/<local_id>')
def dataview(local_id):
    series = Series.query.filter_by(measurements=local_id, empty=False).all()
    s_fields = ('id','date','siteid','subsiteid','point',
                'start_time','end_time','start_temp','end_temp',
                'chamber_vol','chamber_area',
                'co2','ch4','n2o',
                'trim_head','trim_tail',
                'slope_co2','intercept_co2','flux_lin_co2','rsum_co2','rar_co2',
                'slope_ch4','intercept_ch4','flux_lin_ch4','rsum_ch4','rar_ch4',
                'slope_n2o','intercept_n2o','flux_lin_n2o','rsum_n2o','rar_n2o',
                'bad')
    objs = [series[i].to_dict(only=s_fields) for i in range(len(series))]
    for i in range(len(series)):
        gases = ['time']
        if series[i].co2:
            gases.append('co2_ppm')
        if series[i].ch4:
            gases.append('ch4_ppb')
        if series[i].n2o:
            gases.append('n2o_ppb')
        data = Datum.query.filter_by(series=series[i].id).all()
        dobjs = [data[j].to_dict(only=tuple(gases)) for j in range(len(data))]
        objs[i]['data'] = dobjs
    return render_template('dataview.html', data = objs)

## flux calculation api routes

@server.route('/api/calculate_all_fluxes')
def calculate_all_fluxes():
    all_series = Series.query.all()
    for j in range(len(all_series)):
        series = all_series[j]
        if not series.empty:
            ##if series.flux_lin_co2 > 0.0:
            ##    continue
            series_id = series.id
            data = Datum.query.filter_by(series=series_id).all()
            if len(data) > 1:
                if series.co2:
                    date = series.date
                    fvs_co2 = co2_lin_flux(data,series.start_temp,series.end_temp,
                                        series.chamber_vol,series.chamber_area,
                                        series.trim_head,series.trim_tail,date)
                    series.flux_lin_co2 = fvs_co2.lin_flux
                    series.intercept_co2 = fvs_co2.intercept
                    series.slope_co2 = fvs_co2.slope
                    series.rsum_co2 = fvs_co2.residual_sum
                    series.rar_co2 = fvs_co2.residual_ar
                    db.session.commit()
                if series.ch4:
                    date = series.date
                    fvs_ch4 = ch4_lin_flux(data,series.start_temp,series.end_temp,
                                           series.chamber_vol,series.chamber_area,
                                           series.trim_head,series.trim_tail,date)
                    series.flux_lin_ch4 = fvs_ch4.lin_flux
                    series.intercept_ch4 = fvs_ch4.intercept
                    series.slope_ch4 = fvs_ch4.slope
                    series.rsum_ch4 = fvs_ch4.residual_sum
                    series.rar_ch4 = fvs_ch4.residual_ar
                    db.session.commit()
                if series.n2o:
                    date = series.date
                    fvs_n2o = n2o_lin_flux(data,series.start_temp,series.end_temp,
                                           series.chamber_vol,series.chamber_area,
                                           series.trim_head,series.trim_tail,date)
                    series.flux_lin_n2o = fvs_n2o.lin_flux
                    series.intercept_n2o = fvs_n2o.intercept
                    series.slope_n2o = fvs_n2o.slope
                    series.rsum_n2o = fvs_n2o.residual_sum
                    series.rar_n2o = fvs_n2o.residual_ar
                    db.session.commit()
            else:
                print("Warning: no data in non-empty Series " + str(series_id))
                flash("Warning: no data in non-empty Series " + str(series_id))
    return redirect("/maintenance")

@server.route('/api/calculate_fluxes/<local_id>')
def calculate_fluxes(local_id):
    rel_series = Series.query.filter_by(measurements=local_id).all()
    for j in range(len(rel_series)):
        series = rel_series[j]
        if not series.empty:
            series_id = series.id
            data = Datum.query.filter_by(series=series_id).all()
            if len(data) > 1:
                if series.co2:
                    date = series.date
                    fvs_co2 = co2_lin_flux(data,series.start_temp,series.end_temp,
                                           series.chamber_vol,series.chamber_area,
                                           series.trim_head,series.trim_tail,date)
                    series.flux_lin_co2 = fvs_co2.lin_flux
                    series.intercept_co2 = fvs_co2.intercept
                    series.slope_co2 = fvs_co2.slope
                    series.rsum_co2 = fvs_co2.residual_sum
                    series.rar_co2 = fvs_co2.residual_ar
                    db.session.commit()
                if series.ch4:
                    date = series.date
                    fvs_ch4 = ch4_lin_flux(data,series.start_temp,series.end_temp,
                                           series.chamber_vol,series.chamber_area,
                                           series.trim_head,series.trim_tail,date)
                    series.flux_lin_ch4 = fvs_ch4.lin_flux
                    series.intercept_ch4 = fvs_ch4.intercept
                    series.slope_ch4 = fvs_ch4.slope
                    series.rsum_ch4 = fvs_ch4.residual_sum
                    series.rar_ch4 = fvs_ch4.residual_ar
                    db.session.commit()
                if series.n2o:
                    date = series.date
                    fvs_n2o = n2o_lin_flux(data,series.start_temp,series.end_temp,
                                           series.chamber_vol,series.chamber_area,
                                           series.trim_head,series.trim_tail,date)
                    series.flux_lin_n2o = fvs_n2o.lin_flux
                    series.intercept_n2o = fvs_n2o.intercept
                    series.slope_n2o = fvs_n2o.slope
                    series.rsum_n2o = fvs_n2o.residual_sum
                    series.rar_n2o = fvs_n2o.residual_ar
                    db.session.commit()
            else:
                print("Warning: no data in non-empty Series " + str(series_id))
                flash("Warning: no data in non-empty Series " + str(series_id))
    return redirect("/maintenance")

@server.route('/api/calculate_fluxes2/<local_id>')
def calculate_fluxes2(local_id):
    rel_series = Series.query.filter_by(measurements=local_id).all()
    for j in range(len(rel_series)):
        series = rel_series[j]
        if not series.empty:
            series_id = series.id
            data = Datum.query.filter_by(series=series_id).all()
            if len(data) > 1:
                if series.co2:
                    date = series.date
                    fvs_co2 = co2_lin_flux(data,series.start_temp,series.end_temp,
                                           series.chamber_vol,series.chamber_area,
                                           series.trim_head,series.trim_tail,date)
                    series.flux_lin_co2 = fvs_co2.lin_flux
                    series.intercept_co2 = fvs_co2.intercept
                    series.slope_co2 = fvs_co2.slope
                    series.rsum_co2 = fvs_co2.residual_sum
                    series.rar_co2 = fvs_co2.residual_ar
                    db.session.commit()
                if series.ch4:
                    date = series.date
                    fvs_ch4 = ch4_lin_flux(data,series.start_temp,series.end_temp,
                                           series.chamber_vol,series.chamber_area,
                                           series.trim_head,series.trim_tail,date)
                    series.flux_lin_ch4 = fvs_ch4.lin_flux
                    series.intercept_ch4 = fvs_ch4.intercept
                    series.slope_ch4 = fvs_ch4.slope
                    series.rsum_ch4 = fvs_ch4.residual_sum
                    series.rar_ch4 = fvs_ch4.residual_ar
                    db.session.commit()
                if series.n2o:
                    date = series.date
                    fvs_n2o = n2o_lin_flux(data,series.start_temp,series.end_temp,
                                           series.chamber_vol,series.chamber_area,
                                           series.trim_head,series.trim_tail,date)
                    series.flux_lin_n2o = fvs_n2o.lin_flux
                    series.intercept_n2o = fvs_n2o.intercept
                    series.slope_n2o = fvs_n2o.slope
                    series.rsum_n2o = fvs_n2o.residual_sum
                    series.rar_n2o = fvs_n2o.residual_ar
                    db.session.commit()
            else:
                print("Warning: no data in non-empty Series " + str(series_id))
                flash("Warning: no data in non-empty Series " + str(series_id))
    return True

@server.route('/api/calculate_series_fluxes2/<sid>')
def calculate_series_fluxes2(sid):
    series = Series.query.filter_by(id=int(sid)).first()
    if not series.empty:
        series_id = series.id
        data = Datum.query.filter_by(series=series_id).all()
        if len(data) > 1:
            if series.co2:
                date = series.date
                fvs_co2 = co2_lin_flux(data,series.start_temp,series.end_temp,
                                       series.chamber_vol,series.chamber_area,
                                       series.trim_head,series.trim_tail,date)
                series.flux_lin_co2 = fvs_co2.lin_flux
                series.intercept_co2 = fvs_co2.intercept
                series.slope_co2 = fvs_co2.slope
                series.rsum_co2 = fvs_co2.residual_sum
                series.rar_co2 = fvs_co2.residual_ar
                db.session.commit()
            if series.ch4:
                date = series.date
                fvs_ch4 = ch4_lin_flux(data,series.start_temp,series.end_temp,
                                       series.chamber_vol,series.chamber_area,
                                       series.trim_head,series.trim_tail,date)
                series.flux_lin_ch4 = fvs_ch4.lin_flux
                series.intercept_ch4 = fvs_ch4.intercept
                series.slope_ch4 = fvs_ch4.slope
                series.rsum_ch4 = fvs_ch4.residual_sum
                series.rar_ch4 = fvs_ch4.residual_ar
                db.session.commit()
            if series.n2o:
                date = series.date
                fvs_n2o = n2o_lin_flux(data,series.start_temp,series.end_temp,
                                       series.chamber_vol,series.chamber_area,
                                       series.trim_head,series.trim_tail,date)
                series.flux_lin_n2o = fvs_n2o.lin_flux
                series.intercept_n2o = fvs_n2o.intercept
                series.slope_n2o = fvs_n2o.slope
                series.rsum_n2o = fvs_n2o.residual_sum
                series.rar_n2o = fvs_n2o.residual_ar
                db.session.commit()
        else:
            print("Warning: no data in non-empty Series " + str(series_id))
            flash("Warning: no data in non-empty Series " + str(series_id))
    return True


## ajax routes

@server.route('/ajax/recalculate',methods=['POST'])
def calculate_flux():
    out = {'ok': False}
    series_id = request.json.get('series_id')
    start_value = request.json.get('trim_start_value')
    end_value = request.json.get('trim_end_value')
    print("id " + str(series_id) + " vs " + str(start_value) + " ve " + str(end_value))
    series = Series.query.filter_by(id=series_id).first()
    data = Datum.query.filter_by(series=series_id).all()
    date = series.date
    if series.co2:
        fvs_co2 = co2_lin_flux(data,series.start_temp,series.end_temp,series.chamber_vol,
                               series.chamber_area,start_value,end_value,date)
        series.flux_lin_co2  = fvs_co2.lin_flux
        series.intercept_co2 = fvs_co2.intercept
        series.slope_co2     = fvs_co2.slope
        series.rsum_co2      = fvs_co2.residual_sum
        series.rar_co2       = fvs_co2.residual_ar
        out['new_slope_co2']     = fvs_co2.slope
        out['new_intercept_co2'] = fvs_co2.intercept
        out['new_lin_flux_co2']  = round(fvs_co2.lin_flux,3)
        out['new_rsum_co2']      = round(fvs_co2.residual_sum,1)
        out['new_rar_co2']       = round(fvs_co2.residual_ar,1)
        out['ok'] = True
    else:
        out['new_slope_co2']     = 0.0
        out['new_intercept_co2'] = 0.0
        out['new_lin_flux_co2']  = 0.0
        out['new_rsum_co2']      = 0.0
        out['new_rar_co2']       = 0.0
    if series.ch4:
        fvs_ch4 = ch4_lin_flux(data,series.start_temp,series.end_temp,series.chamber_vol,
                               series.chamber_area,start_value,end_value,date)
        series.flux_lin_ch4  = fvs_ch4.lin_flux
        series.intercept_ch4 = fvs_ch4.intercept
        series.slope_ch4     = fvs_ch4.slope
        series.rsum_ch4      = fvs_ch4.residual_sum
        series.rar_ch4       = fvs_ch4.residual_ar
        out['new_slope_ch4']     = fvs_ch4.slope
        out['new_intercept_ch4'] = fvs_ch4.intercept
        out['new_lin_flux_ch4']  = round(fvs_ch4.lin_flux,3)
        out['new_rsum_ch4']      = round(fvs_ch4.residual_sum,1)
        out['new_rar_ch4']       = round(fvs_ch4.residual_ar,1)
        out['ok'] = True
    else:
        out['new_slope_ch4']     = 0.0
        out['new_intercept_ch4'] = 0.0
        out['new_lin_flux_ch4']  = 0.0
        out['new_rsum_ch4']      = 0.0
        out['new_rar_ch4']       = 0.0
    if series.n2o:
        fvs_n2o = n2o_lin_flux(data,series.start_temp,series.end_temp,series.chamber_vol,
                               series.chamber_area,start_value,end_value,date)
        series.flux_lin_n2o  = fvs_n2o.lin_flux
        series.intercept_n2o = fvs_n2o.intercept
        series.slope_n2o     = fvs_n2o.slope
        series.rsum_n2o      = fvs_n2o.residual_sum
        series.rar_n2o       = fvs_n2o.residual_ar
        out['new_slope_n2o']     = fvs_n2o.slope
        out['new_intercept_n2o'] = fvs_n2o.intercept
        out['new_lin_flux_n2o']  = round(fvs_n2o.lin_flux,3)
        out['new_rsum_n2o']      = round(fvs_n2o.residual_sum,1)
        out['new_rar_n2o']       = round(fvs_n2o.residual_ar,1)
        out['ok'] = True
    else:
        out['new_slope_n2o']     = 0.0
        out['new_intercept_n2o'] = 0.0
        out['new_lin_flux_n2o']  = 0.0
        out['new_rsum_n2o']      = 0.0
        out['new_rar_n2o']       = 0.0
    series.trim_head = start_value
    series.trim_tail = end_value
    db.session.commit()
    return jsonify(out)

@server.route('/ajax/switch_bad/<series_id>',methods=['GET'])
def switch(series_id):
    s = Series.query.filter_by(id=series_id).first()
    out = {}
    if s.bad:
        s.bad = False
        out['new_value'] = False
    else:
        s.bad = True
        out['new_value'] = True
    db.session.commit()
    return jsonify(out)

## api routes for the R package

## get a list of available siteids from all Series
@server.route('/api/get/siteids',methods=['GET'])
@login_required
def get_siteids():
    siteids_query = Series.query.with_entities(Series.siteid).distinct()
    if siteids_query:
        siteids = [row.siteid for row in siteids_query.all()]
    else:
        siteids = {'msg': 'no siteids on server'}
    return jsonify(siteids)

## get all Series for a specific siteid
@server.route('/api/get/series_by_siteid/<site_id>',methods=['GET'])
@login_required
def get_series_siteid(site_id):
    series = Series.query.filter_by(siteid=site_id).all()
    if series:
        out = []
        for s in series:
            if not s.empty:
                out.append({'id': s.id,
                            'date': s.date.strftime('%Y-%m-%d'),
                            'siteid': s.siteid, 'subsiteid': s.subsiteid, 'point': s.point,
                            'start_time': s.start_time.strftime('%H:%M:%S'),
                            'end_time': s.end_time.strftime('%H:%M:%S'),
                            'start_ppm': s.start_ppm,'end_ppm': s.end_ppm,
                            'start_temp': s.start_temp, 'end_temp': s.end_temp,
                            'chamber_vol': s.chamber_vol, 'chamber_area': s.chamber_area,
                            'wt': s.wt, 't05': s.t05, 't10': s.t10, 't15': s.t15, 
                            't20': s.t20, 't30': s.t30, 't40': s.t40,
                            'co2': s.co2, 'ch4': s.ch4, 'n2o': s.n2o,
                            'trim_head': s.trim_head, 'trim_tail': s.trim_tail,
                            'flux_lin_co2': s.flux_lin_co2,
                            'flux_lin_ch4': s.flux_lin_ch4,
                            'flux_lin_n2o': s.flux_lin_n2o,
                            'rsum_co2': s.rsum_co2, 'rar_co2': s.rar_co2,
                            'rsum_ch4': s.rsum_ch4, 'rar_ch4': s.rar_ch4,
                            'rsum_n2o': s.rsum_n2o, 'rar_n2o': s.rar_n2o,
                            'notes1': s.notes1, 'notes2': s.notes2, 'notes3': s.notes3,
                            'bad': s.bad})
    else:
        out = {'msg': 'no series with site id ' + site_id}
    return jsonify(out)

@server.route('/api/get/data_by_series_id/<series_id>',methods=['GET'])
@login_required
def get_data_series_id(series_id):
    out = {}
    series = Series.query.filter_by(id=series_id).first()
    if series and not series.empty:
        data = Datum.query.filter_by(series=series_id).all()
        out['trim_head'] = series.trim_head
        out['trim_tail'] = series.trim_tail
        out['slope_co2'] = series.slope_co2
        out['slope_ch4'] = series.slope_ch4
        out['slope_n2o'] = series.slope_n2o
        out['intercept_co2'] = series.intercept_co2
        out['intercept_ch4'] = series.intercept_ch4
        out['intercept_n2o'] = series.intercept_n2o
        out['flux_lin_co2'] = series.flux_lin_co2
        out['flux_lin_ch4'] = series.flux_lin_ch4
        out['flux_lin_n2p'] = series.flux_lin_n2o
        if data:
            out['time'] = [data[i].time.strftime('%H:%M:%S')
                           for i in range(len(data))]
            if series.co2:
                out['co2_ppm'] = [data[i].co2_ppm for i in range(len(data))]
            else:
                out['co2_ppm'] = []
            if series.ch4:
                out['ch4_ppb'] = [data[i].ch4_ppb for i in range(len(data))]
            else:
                out['ch4_ppb'] = []
            if series.n2o:
                out['n2o_ppb'] = [data[i].n2o_ppb for i in range(len(data))]
            else:
                out['n2o_ppb'] = []
        else:
            out['time'] = []
            out['co2_ppm'] = []
            out['ch4_ppm'] = []
            out['n2o_ppm'] = []
            out['msg'] = 'no data in series id ' + str(series_id)
    else:
        out['msg'] = 'no data with series id ' + str(series_id)
    return jsonify(out)
