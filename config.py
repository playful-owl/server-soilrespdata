import os

basedir = os.path.abspath(os.path.dirname(__file__))
fileserver_url: str = os.environ['FILESERVERURL']

class configuration(object):
    SECRET_KEY = 'avainsalaisuusavain'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
